### 10.7.3  seL4_ARM_IOPageTable

#### 10.7.3.2  Unmap

static inline int seL4_ARM_IOPageTable_Unmap

TODO

类型 | 名字 | 描述
--- | --- | ---
seL4_ARM_IOPageTable | _service | 要操作的I/O页表能力句柄。自当前线程根CNode按机器字位数解析

*返回值*：返回0表示成功，非0值表示有错误发生。第10.1节描述了发生错误时消息寄存器和消息标签有关内容。

*描述*：TODO
